﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Script_PlayerSync : NetworkBehaviour {

    [SyncVar]
    Vector3 syncedPosition;     // Player position recognized in the server
    [SyncVar]
    Quaternion syncedRotation;  // Player rotation recognized in the server

    #region variables
    [Header("\tReference Values")]
    public Transform myTransform;       // Player transform

    [Header("\tValues for Client Management")]
    [Header("Player")]
    public Rigidbody myRigidbody;       // Player rigidbody
    public CapsuleCollider myCollider;  // Player capsule collider
    public UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController myController; // Player First-Person controller
    [Header("Camera")]
    public GameObject myCameraObject;   // Player camera as a game object
    public Camera myCamera;             // player camera
    public AudioListener myListener;    // player audio listener

    [Header("\tSync Values")]
    [Header("Rotation")]
    public float rotationLerpRate = 15f;
    public float rotationThreshold = 5f;
    [Header("Position")]
    public float positionLerpRate = 15f;
    public float positionThreshold = 0.3f;

    Quaternion lastPlayerRotation;
    Vector3 lastPlayerPosition;
    #endregion

    void Start()
    {
        if (!isLocalPlayer)
        {
            Destroy(myController);
            Destroy(myRigidbody);
            Destroy(myCollider);
            Destroy(myCameraObject);
        }
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            TransmitPosition();
            TransmitRotation();
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (!isLocalPlayer)
        {
            LerpPosition();
            LerpRotation();
        }
	}

    #region rotation
    [Client]
    void TransmitRotation()
    {
        if (Quaternion.Angle(myTransform.rotation, lastPlayerRotation) > rotationThreshold)
        {
            CmdSendRotationToServer(myTransform.rotation);
            lastPlayerRotation = myTransform.rotation;
        }
    }

    [Command]
    void CmdSendRotationToServer(Quaternion rotationToSend)
    {
        syncedRotation = rotationToSend;
    }


    void LerpRotation()
    {
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, syncedRotation, Time.deltaTime * rotationLerpRate);
    }
    #endregion

    #region position
    [Client]
    void TransmitPosition()
    {
        if (Vector3.Distance(myTransform.position, lastPlayerPosition) > positionThreshold)
        {
            CmdSendPositionToServer(myTransform.position);
            lastPlayerPosition = myTransform.position;
        }
    }

    [Command]
    void CmdSendPositionToServer(Vector3 positionToSend)
    {
        syncedPosition = positionToSend;
    }

    void LerpPosition()
    {
        myTransform.position = Vector3.Lerp(myTransform.position, syncedPosition, Time.deltaTime * positionLerpRate);
    }
    #endregion
}
