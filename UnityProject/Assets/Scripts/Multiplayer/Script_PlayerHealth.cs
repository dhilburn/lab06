﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Script_PlayerHealth : NetworkBehaviour 
{
    /****************************************************************
     *  NOTE: Most SerializeField tags used for debugging purposes. *
     *        If it is not commented out, it is intentional.        *
     *        if it is, SerializeField is ONLY used for debugging!  *
     ****************************************************************/

    [SyncVar]
    //[SerializeField]
    int syncedHealth;               // Server-side player health   
    [SyncVar]
    string syncedName;

    [SerializeField]
    string playerName = "Player"; 
    //[SerializeField]
    Text playerNameText;            // Text field for player name

    [Tooltip("How much damage player will heal when Heal Damage is pressed")]
    public int healAmount;
    [Tooltip("How much damage player will take when Take Damage is pressed")]
    public int damageAmount;        

    //[SerializeField]
    int playerHealth = 100;         // Client-side player health
    //[SerializeField]
    Text healthText;                // text field for player health

    //[SerializeField]
    Button healHealth;              // Button that causes player to heal damage
    //[SerializeField]
    Button takeDamage;              // Button that causes player to take damage


	// Use this for initialization
	void Start () 
    {
        // Assign the name based on what's stored on the network manager
        
        playerName = GameObject.Find("Network Manager").GetComponent<Script_PlayerName>().playerName;
        if (playerName.Equals(""))
            playerName = "Player";

        // Initialize name and health in the server
        SetName();
        SetHealth();

        // Get the text components for player name and health and assign to them
        playerNameText = GameObject.Find("Player_Name_Text").GetComponent<Text>();
        healthText = GameObject.Find("Player_Health_Text").GetComponent<Text>();
        playerNameText.text = playerName;
        healthText.text = playerHealth.ToString();

        // Get the buttons for taking and healing damage
        healHealth = GameObject.Find("Button_Heal").GetComponent<Button>();
        takeDamage = GameObject.Find("Button_Damage").GetComponent<Button>();
        
        // Add listeners for the buttons. These will call their respective functions for healing and taking damage.
        healHealth.onClick.AddListener(HealDamage);
        takeDamage.onClick.AddListener(TakeDamage);       
        
        // Write debug statements that track the player's health
        InvokeRepeating("HealthLog", 0f, 2f);   
	}

    void Update()
    {
        UpdateHealth();
    }

    void UpdateHealth()
    {
        if (playerHealth > 80)
            healthText.color = Color.green;
        else if (playerHealth > 50)
            healthText.color = Color.yellow;
        else
            healthText.color = Color.red;

        healthText.text = playerHealth.ToString();
    }

    void HealthLog()
    {
        Debug.Log(syncedName + " still has " + syncedHealth + " HP");
        if (syncedHealth <= 0)
            Debug.Log("YOU ARE DEAD. NOT BIG SURPRISE.");
    }

    [Client]
    public void SetName()
    {
        if (isLocalPlayer)
        {
            CmdSendNameToServer(playerName);
        }
    }

    [Client]
    public void SetHealth()
    {
        if(isLocalPlayer)
        {
            CmdSendHealthToServer(playerHealth);
        }
    }

    [Client]
    public void HealDamage()
    {
        if(isLocalPlayer)
        {
            if (playerHealth < 100)
            {
                playerHealth += healAmount;
                CmdSendHealthToServer(playerHealth);
            }
            else
                //Debug.Log("You are at max health!");
                Debug.Log(playerNameText.text + " is at max health!");
        }
    }

    [Client]
    public void TakeDamage()
    {
        if(isLocalPlayer)
        {
            if (playerHealth > 0)
            {
                playerHealth -= damageAmount;
                CmdSendHealthToServer(playerHealth);
            }
            else
                //Debug.Log("You are a living dead person!");
                Debug.Log(playerNameText.text + " is a living dead person!");
        }
    }
    
    [Command]
    void CmdSendHealthToServer(int health)
    {
        syncedHealth = health;
    }

    [Command]
    void CmdSendNameToServer(string name)
    {
        syncedName = name;
    }
}