﻿using UnityEngine;
using System.Collections;

public class SpawnPointGizmos : MonoBehaviour 
{
    public float radius = 0.25f;

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, radius);
    }
}
